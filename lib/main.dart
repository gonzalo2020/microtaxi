import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart';
import 'package:taxis/screens/search_places_screen.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      //home: SearchPlacesScreen(),
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Location location = new Location();
  Future _checkGps() async {
    if (!await location.serviceEnabled()) {
      location.requestService();
    }
  }

  Future<bool> checkinternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initTimer();
  }

  void initTimer() async {
    if (await checkinternet()) {
      //open app
      Timer(Duration(seconds: 5), () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => SearchPlacesScreen()));
      });
      bool ison = await location.serviceEnabled();
      if (!ison) {
        //if defvice is off
        bool isturnedon = await location.requestService();
        if (isturnedon) {
        } else {
          exit(0);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width,
        child: FutureBuilder(
          future: checkinternet(),
          builder: (BuildContext c, snap) {
            if (snap.data == null) {
              return Center(
                child: Container(
                    height: 200.0,
                    width: 200.0,
                    child: LottieBuilder.asset('assets/mapanimation.json')),
              );
            } else if (snap.data == true) {
              return Center(
                child: Container(
                    height: 200.0,
                    width: 200.0,
                    child: LottieBuilder.asset('assets/mapanimation.json')),
              );
            } else {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/ver.gif",
                    height: 320.0,
                    width: 320.0,
                  ),
                  Text(
                    "Parece que no estas conectado a internet",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                      "Porfavor verifica tu conexion wifi o plan de datos\n                          e intenta de nuevo"),
                  SizedBox(
                    height: 20,
                  ),
                  MaterialButton(
                    minWidth: 200.0,
                    height: 40.0,
                    onPressed: () {
                      setState(() {
                        initTimer();
                      });
                    },
                    color: Colors.yellow,
                    child: const Text('Volver a intentar',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18)),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
