import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class searchPlacescontroller {
  void onMapCreated(GoogleMapController controller) {}
  late bool _gpsEnabled;
  bool get gpsEnabled => _gpsEnabled;

  Future<void> _init() async {
    _gpsEnabled = await Geolocator.isLocationServiceEnabled();
  }
}
