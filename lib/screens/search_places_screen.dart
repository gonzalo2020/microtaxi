import 'package:flutter/material.dart';
import 'package:flutter_google_places_hoc081098/flutter_google_places_hoc081098.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:url_launcher/url_launcher.dart';

//var numero = "593999213947";
var numero = "593991162255";
var encomienda =
    '"Enviar encomienda a la siguiente direccion"+"https://maps.google.com/?q=-0.3301018,-78.451295"+\n+Pedido:+\n+$pedido';
var pedido;
var ubiLati;
var ubiLong;

class SearchPlacesScreen extends StatefulWidget {
  const SearchPlacesScreen({Key? key}) : super(key: key);

  @override
  State<SearchPlacesScreen> createState() => _SearchPlacesScreenState();
}

const kGoogleApiKey = 'AIzaSyBRcYuK1xj3NsLlT46VMffcsanY-SQvfL4';
final homeScaffoldKey = GlobalKey<ScaffoldState>();

class _SearchPlacesScreenState extends State<SearchPlacesScreen> {
  static const CameraPosition initialCameraPosition =
      CameraPosition(target: LatLng(-0.3301018, -78.451295), zoom: 14.0);

  String location = "Buscar dirección";

  Set<Marker> markersList = {};

  late GoogleMapController googleMapController;

  final Mode _mode = Mode.overlay;

  late bool _gpsEnabled;
  bool get gpsEnabled => _gpsEnabled;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      key: homeScaffoldKey,
      body: Stack(
        children: [
          GoogleMap(
            initialCameraPosition: initialCameraPosition,
            markers: markersList,
            mapToolbarEnabled: false,
            zoomControlsEnabled: false,
            mapType: MapType.normal,
            onMapCreated: (GoogleMapController controller) {
              googleMapController = controller;
            },
          ),
          Positioned(
            //search input bar
            top: 25,
            child: InkWell(
              onTap: () async {
                var place = await PlacesAutocomplete.show(
                    context: context,
                    apiKey: kGoogleApiKey,
                    mode: Mode.overlay,
                    types: [],
                    strictbounds: false,
                    components: [Component(Component.country, 'ec')],
                    language: 'es-419',
                    //google_map_webservice package
                    onError: (err) {
                      print(err);
                    });

                if (place != null) {
                  setState(() {
                    location = place.description.toString();
                  });

                  //form google_maps_webservice package
                  final plist = GoogleMapsPlaces(
                    apiKey: kGoogleApiKey,
                    apiHeaders: await GoogleApiHeaders().getHeaders(),
                    //from google_api_headers package
                  );
                  String placeid = place.placeId ?? "0";
                  final detail = await plist.getDetailsByPlaceId(placeid);
                  final geometry = detail.result.geometry!;
                  final lat = detail.result.geometry!.location.lat;
                  final lng = detail.result.geometry!.location.lng;
                  ubiLati = detail.result.geometry!.location.lat;
                  ubiLong = detail.result.geometry!.location.lng;

                  markersList.clear();
                  markersList.add(Marker(
                      markerId: const MarkerId("0"),
                      position: LatLng(lat, lng),
                      infoWindow: InfoWindow(title: detail.result.name)));
                  setState(() {});
                  googleMapController.animateCamera(
                      CameraUpdate.newLatLngZoom(LatLng(lat, lng), 14.0));
                }
              },
              child: Padding(
                padding: EdgeInsets.all(15),
                child: Card(
                  child: Container(
                    width: 320,
                    height: 70,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              Color.fromARGB(255, 19, 22, 19),
                              Colors.yellow.shade600,
                            ])),
                    child: Container(
                        width: 315,
                        height: 65,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Colors.white,
                        ),
                        alignment: Alignment.center,
                        child: ListTile(
                          title: Text(
                            location,
                            style: TextStyle(fontSize: 18),
                          ),
                          trailing: Icon(Icons.search),
                          dense: true,
                        )),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(
          bottom: 250.0,
        ),
        child: FloatingActionButton(
          child: Icon(Icons.gps_fixed, color: Colors.red, size: 33),
          //person_pin_circle
          backgroundColor: Colors.white,
          onPressed: () async {
            Position position = await _determinePosition();

            googleMapController.animateCamera(CameraUpdate.newCameraPosition(
                CameraPosition(
                    target: LatLng(position.latitude, position.longitude),
                    zoom: 14)));
            ubiLati = position.latitude;
            ubiLong = position.longitude;
            markersList.clear();
            markersList.add(Marker(
                markerId: const MarkerId('0'),
                infoWindow: InfoWindow(title: "Mi ubicacion"),
                position: LatLng(position.latitude, position.longitude)));
            setState(() {});
          },
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromARGB(255, 255, 255, 255),
                  ),
                ),
                onPressed: () async {
                  setState(() {
                    print("222");
                    if (ubiLati == null) {
                      showAlertDialog(context);
                      print("ingresaaaaa");
                    } else {
                      var a = ubiLati;
                      var b = ubiLong;
                      var text =
                          '"Enviar unidad a la siguiente direccion"+ direccion+"https://maps.google.com/?q=$a,$b"';
                      launchUrl(Uri.parse('https://wa.me/$numero?text=$text'),
                          mode: LaunchMode.externalApplication);
                    }
                  });
                },
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Icon(Icons.drive_eta, color: Colors.amber, size: 29),
                    Text(
                      'Taxi',
                      style: TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0),
                          //fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                  ],
                )),
            ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromARGB(255, 255, 255, 255),
                  ),
                ),
                onPressed:
                    //buttonenabled ?
                    () async {
                  setState(() {
                    print("222");
                    if (ubiLati == null) {
                      showAlertDialog1(context);

                      print("ingresaaaaa");
                    } else {
                      print("saleeeee");
                      Encomienda(context).then((onValue) async {});
                    }
                  });
                },
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Icon(Icons.shopping_bag, color: Colors.red, size: 29),
                    Text(
                      'Encomienda',
                      style: TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0),
                          //fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                  ],
                )
                // : null,
                ),
            ElevatedButton(
              onPressed: () {
                informacion(context);
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  Color.fromARGB(255, 255, 255, 255),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const [
                  Icon(Icons.live_help_outlined,
                      color: Colors.blueAccent, size: 29),
                  Text(
                    'Informacion',
                    style: TextStyle(
                        color: Color.fromARGB(255, 0, 0, 0),
                        //fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _handlePressButton() async {
    Prediction? p = await PlacesAutocomplete.show(
        context: context,
        apiKey: kGoogleApiKey,
        onError: onError,
        mode: _mode,
        language: 'es-419',
        strictbounds: false,
        types: [""],
        textDecoration: InputDecoration(
            hintText: 'Search',
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(color: Colors.white))),
        components: [Component(Component.country, "ec")]);

    displayPrediction(p!, homeScaffoldKey.currentState);
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();

    if (!serviceEnabled) {
      return Future.error('Location services are disabled');
    }

    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied) {
        return Future.error("Location permission denied");
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied');
    }

    Position position = await Geolocator.getCurrentPosition();

    return position;
  }

  void onError(PlacesAutocompleteResponse response) {
    // homeScaffoldKey.currentState!.showSnackBar(SnackBar(content: Text(response.errorMessage!)));
  }

  Future<void> displayPrediction(
      Prediction p, ScaffoldState? currentState) async {
    GoogleMapsPlaces places = GoogleMapsPlaces(
        apiKey: kGoogleApiKey,
        apiHeaders: await const GoogleApiHeaders().getHeaders());

    PlacesDetailsResponse detail = await places.getDetailsByPlaceId(p.placeId!);

    final lat = detail.result.geometry!.location.lat;
    final lng = detail.result.geometry!.location.lng;

    markersList.clear();
    markersList.add(Marker(
        markerId: const MarkerId("0"),
        position: LatLng(lat, lng),
        infoWindow: InfoWindow(title: detail.result.name)));

    setState(() {});

    googleMapController
        .animateCamera(CameraUpdate.newLatLngZoom(LatLng(lat, lng), 14.0));
  }

  void informacion(BuildContext context) {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        context: context,
        builder: (_) {
          return DraggableScrollableSheet(
              initialChildSize: 0.9, //set this as you want
              maxChildSize: 1, //set this as you want
              minChildSize: 0.5, //set this as you want
              builder: (context, scrollController) {
                return Container(
                  decoration: BoxDecoration(
                      color: Color(0xFFF5F5F5),
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(20))),
                  padding: const EdgeInsets.all(16),
                  child: ListView(
                    controller: scrollController,
                    children: [
                      Image.asset(
                        'assets/taxi.png',
                        height: 500,
                        width: 600,
                      ),
                      Text(
                        '\n Servicio de Taxi\n ',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '1.	Activar ubicación del dispositivo\n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        '2.	Mostrar ubicación actual o ingrese una\n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        '    dirección\n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Icon(Icons.gps_fixed, color: Colors.red, size: 27),
                      Text(
                        '3.	Pedir  Taxi  \n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Icon(Icons.drive_eta, color: Colors.amber, size: 27),
                      Text(
                        '\n \n Servicio de Ecomienda\n',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '1.	Activar ubicación del dispositivo\n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        '2.	Mostrar ubicación actual o ingrese una \n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        '    dirección\n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Icon(Icons.gps_fixed, color: Colors.red, size: 27),
                      Text(
                        '3.	Pedir Ecomienda   \n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Icon(Icons.shopping_bag, color: Colors.red, size: 27),
                      Text(
                        '4.	Ingrese su pedido\n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        '\n Uso de la aplicación\n ',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Conexión a red WiFi o datos móviles.\n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        'Activar ubicación del celular en las configuraciónes.\n',
                        style: TextStyle(fontSize: 16),
                      ),
                      Center(
                        child: Text('Elaborado por:'),
                      ),
                      Center(
                        child: Text('JEES EC ESTUDIO® \n \n \n'),
                      ),
                      Center(
                        child: ElevatedButton(
                          child: Text('Cerrar'),
                          onPressed: () => Navigator.of(context).pop(),
                        ),
                      )
                    ],
                  ),
                );
              });
        });
  }

  Encomienda(BuildContext context) {
    TextEditingController customcontroller = TextEditingController();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Ingrese el pedido"),
            content: TextField(
              controller: customcontroller,
              maxLines: 5,
              minLines: 1,
            ),
            actions: [
              ElevatedButton(
                //textColor: Colors.black,
                onPressed: () =>
                    Navigator.of(context, rootNavigator: true).pop(),
                //onPressed: () => Navigator.pop(context, false),
                //
                child: Text(
                  'CANCELAR',
                  style: TextStyle(
                    color: Colors.black87,
                  ),
                ),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.amberAccent),
                ),
              ),
              ElevatedButton(
                //textColor: Color(0xFF6200EE),
                //onPressed: () => Navigator.pop(context, true),
                onPressed: () async {
                  pedido = customcontroller.text;
                  var c = ubiLati;
                  var d = ubiLong;
                  var encomienda1 =
                      '"Enviar encomienda a la siguiente direccion"+"https://maps.google.com/?q=$c,$d"+\n+Pedido:+\n+$pedido';

                  Navigator.of(context).pop(customcontroller.text.toString());
                  print("///////////////////////////////////////");
                  print(customcontroller.text);
                  launchUrl(
                      Uri.parse('https://wa.me/$numero?text=$encomienda1'),
                      mode: LaunchMode.externalApplication);
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.amberAccent),
                ),
                child: Text(
                  'ACEPTAR',
                  style: TextStyle(
                    color: Colors.black87,
                  ),
                ),
              ),
            ],
          );
        }).then((exit) {
      if (exit == null) return;

      if (exit) {
        onPressed:
        () => Navigator.of(context).pop();
      } else {
        onPressed:
        () {
          launchUrl(Uri.parse('https://wa.me/$numero?text=$encomienda'),
              mode: LaunchMode.externalApplication);
          Navigator.of(context).pop(customcontroller.text.toString());
          print("++++++++++++++++++++++++++++++++++++++");
          print(customcontroller.text);
        };
      }
    });
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () => Navigator.of(context).pop(),
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Servicio de Taxi"),
      content: Text(
          "1.	Activar ubicación del \n    dispositivo\n2.	Mostrar ubicación actual o\n    ingrese una dirección\n 3.	Pedir  Taxi"),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAlertDialog1(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () => Navigator.of(context).pop(),
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Servicio de Ecomienda"),
      content: Text(
          "1.	Activar ubicación del \n    dispositivo\n 2.	Mostrar ubicación actual o\n    ingrese una dirección\n 3.	Pedir encomienda\n 4.	Ingrese su pedido"),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
