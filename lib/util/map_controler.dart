import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:taxis/util/map_style.dart';

class mapcontroler {
  void onMapCreated(GoogleMapController controller) {
    controller.setMapStyle(mapStyle);
  }
}
